﻿4. The name ties the parts of mixed modes into one idea.
4. Vardas susieja dalis nuo mišrių modusų į vieną idėją.

Every mixed mode.
Kiekvienas mišrus modusas.

consisting of many.
Susidedantis nuo daug.

distinct simple ideas.
Atskirų paprastų idėjų.

it seems reasonable.
Tai atrodo pagrįsta.

to inquire.
Ištirti.


Whence it has its unity.
Iš kur tai turi savo vienybę.

and how such.
Ir kaip tokia.

a precise multitude.
Tiksli daugybė.

comes to make.
Ateina pagaminti.

but one idea.
Bet vieną idėją.

since that combination.
Nes tas derinys.

does not always.
Daro ne visada.

exist together in nature?
Egzistuoja kartu viduje gamtos?

To which I answer.
Į kurį aš atsakau.

it is plain.
Tai yra aišku.

it has its unity.
Tai turi savo vienį.

from an act.
Iš akto.

of the mind.
Nuo intelekto.

combining those several simple ideas together.
Sudedant tas keletą paprastų idėjų kartu.

and considering them.
Ir traktuojant jas·

as one complex one.
Keip vieną paprastą vieną.

consisting of those parts.
Susidedant nuo tų dalių.

and the mark of this union.
Ir žymė nuo šio vienio.

or that.
Arba kad.

which is looked on.
Kuris yra žiūrėta ant.

generally to complete it.
Bendrai užbaigti tai.

is one name.
Yra vienas vardas.

given to that combination.
Duotas tam deriniui.

For it is by their names.
Juk tai yra per jų vardus.

that men commonly.
Kad žmonės dažniausiai.

regulate their account.
Reguliuojų jų vertinimą.

of their distinct species.
Nuo jų atskirų rūšių.

of mixed modes.
Nuo mišrių modusų.

seldom allowing.
Retai leisdami.

or considering.
Arba traktuodami.

any number of simple ideas.
Bet kokį skaičių nuo paprastų idėjų.

to make one complex one.
Pagaminti vieną sudėtinę vieną.

but such collections.
Bet tokie deriniai.

as there be names for.
Kaip ten būtų vardai dėl.

Thus, though the killing.
Taip, nors žudymas.

of an old man.
nuo seno žmogaus.

be as fit.
Būtų kaip tiktų.

in nature.
Viduje gamtos.

to be united into one complex idea.
Būti sujungta į vieną sudėtinę idėją.

as the killing a man's father.
Kaip nužudymas žmogaus tėvo.

yet, there being no name.
Vis dėl to, ten būsenčio ne vardo.

standing precisely for the one.
Stovinčio tiksliai dėl vieno.

as there is the name.
Kaip ten yra vardas.

of parricide.
Nuo tėvažudžio.

to mark the other.
Žymėti kitą.

it is not taken.
Tai yra ne paimta.

for a particular complex idea.
Dėl įpatingos sudėtinės idėjos.

nor a distinct species.
Nei atskiros rūšies.

of actions.
Nuo veiksmų.

from that of killing.
Iš kad nuo nužudymo.

a young man.
jauno žmogaus.

or any other man.
Arba bet kurio kito žmogaus.
